#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

int AGR2_Traverse_Sequential(const Filer_mfile& m)
{
	Filer_mfile::ConstTraverser t(m, 2);
	//ROTFiler::ConstTraverser t(m, 2);でも問題ない。

	auto[g, c, p, r, ax, ay, x, y, z] =
		m.GetMemberInfos("GroupID", "ChainID", "pl", "rawid", "ax", "ay", "x", "y", "z");

	for (; !t.IsOver(); t.Next())
	{
		fprintf(stderr, "%7lld %7lld %3lld %7lld %7.4lf %7.4lf %9.1lf %9.1lf %9.1lf\n",
				t[g].i64(), t[c].i64(), t[p].i64(), t[r].i64(), t[ax].f64(), t[ay].f64(), t[x].f64(), t[y].f64(), t[z].f64());
	}

	t.InitTraverser(m, 2);
	for (; !t.IsOver(); t.Next(0))
	{
		do
		{
			do
			{
				fprintf(stderr, "%7lld %7lld %3lld %7lld %7.4lf %7.4lf %9.1lf %9.1lf %9.1lf\n",
						t[g].i64(), t[c].i64(), t[p].i64(), t[r].i64(), t[ax].f64(), t[ay].f64(), t[x].f64(), t[y].f64(), t[z].f64());
			} while (t.Next(2));
		} while (t.Next(1));
	}
	return 0;
}