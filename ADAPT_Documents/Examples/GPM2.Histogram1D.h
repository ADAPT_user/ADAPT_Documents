#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/GPM2/GPMDerivative.h>
#include <ADAPT/AGR2Filer/Filer_bt.h>

using namespace adapt::agr2;
using namespace adapt::gpm2;
using namespace adapt::agr2::lit;

int GPM2_Histogram1D(const Filer_bt& bt1, const Filer_bt& bt2)
{
	R_AGR r1(bt1);
	r1.SetVariates("int(phv/10000)");
	HashmapClct h1 = r1.Hash({ {1} });
	R_AGR r2(bt2);
	r2.SetVariates("int(phv/10000)");
	HashmapClct h2 = r2.Hash({ {1} });

	//GPMCanvas::SetGnuplotPath("path to gnuplot");

	GPMCanvas2D g("TestData\\Graphs\\test.png");
	g.SetXRange(14, 32);
	g.SetXLabel("PH");
	g.SetTitle("basetrack PH distribution");
	g.PlotHistogram(h1, plot::title = "basetrack1", plot::color = "light-blue").
		PlotHistogram(h2, plot::title = "basetrack2", plot::color = "light-green");

	return 0;
}