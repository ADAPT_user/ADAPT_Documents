#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

std::tuple<HashmapClct, HashmapClct> AGR2_Hash_Section(const Filer_mfile& m)
{
	R_AGR agr(m);
	agr.SetFilters("isgreatest(size(phv))"_f && "size(phv)>4");
	agr.SetVariates("ax", "ay");
	HashmapClct h = agr.Hash({ { 0.05 }, { 0.05 } });

	HashmapClct p = h.Projection({ { 1 } });
	fprintf(stderr, "dimension = %d", h.GetDimension());

	HashmapClct::ConstTraverser t(p);
	const auto& v0 = p.GetMemberInfo("BinIndex0");
	const auto& v1 = p.GetMemberInfo("NumOfElements");

	for (; !t.IsOver(); t.Next())
	{
		fprintf(stderr, "index[%2lld], num = %lld\n",
				t[v0].i64(), t[v1].i64());
	}

	return std::forward_as_tuple(std::move(h), std::move(p));
}