#include <ADAPT/AGR2/HierarchicalTreeFiler.h>
#include <ADAPT/AGR2Filer/netscan_data_types_ui.h>

using namespace adapt;
using namespace agr2;

ROTFiler* Create(const std::string& filename)
{
	ROTFiler* res = new ROTFiler();
	//BaseTrack要素
	res->AddLayer(0, "phv", ArbValue::Lng, "ax", ArbValue::Dbl, "ay", ArbValue::Dbl,
				  "x", ArbValue::Dbl, "y", ArbValue::Dbl, "z", ArbValue::Dbl,
				  "pl", ArbValue::Lng, "isg", ArbValue::Lng, "zone", ArbValue::Lng, "rawid", ArbValue::Lng);
	//MicroTrack要素
	res->AddLayer(1, "phv_m", ArbValue::Lng, "ax_m", ArbValue::Dbl, "ay_m", ArbValue::Dbl, "z_m", ArbValue::Dbl,
				  "pos_m", ArbValue::Lng, "isg_m", ArbValue::Lng, "zone_m", ArbValue::Lng, "rawid_m", ArbValue::Lng,
				  "row_m", ArbValue::Lng, "col_m", ArbValue::Lng);
	res->VerifyStructure();

	ROTFiler::Branch br(*res);
	Bpos bpos({ 0, 0 });

	FILE* fp = fopen(filename.c_str(), "rb");
	if (fp == nullptr) throw InvalidArg(filename + " not found.");
	base_track_t b;
	while (fread(&b, sizeof(base_track_t), 1, fp) == 1)
	{
		br.SetValues(0, bpos, b.m[0].ph + b.m[1].ph, b.ax, b.ay, b.x, b.y, b.z, b.pl, b.isg, b.zone, b.rawid);
		br.Resize(1, bpos, 2);
		for (uint32_t i = 0; i < 2; ++i)
		{
			bpos[1] = i;
			micro_track_subset_t& m = b.m[i];
			br.SetValues(1, bpos, m.ph, m.ax, m.ay, m.z, m.pos, m.isg, m.zone, m.rawid, m.row, m.col);
		}
		res->PushBranch(br);
	}
	return res;
}

namespace _autoaliases
{
extern "C" __declspec(dllexport) const void* CreatePlugin;
__pragma(section("boostdll", read))
__declspec(allocate("boostdll"))
__declspec(selectany)
const void* CreatePlugin = reinterpret_cast<const void*>(reinterpret_cast<intptr_t>(Create));
}