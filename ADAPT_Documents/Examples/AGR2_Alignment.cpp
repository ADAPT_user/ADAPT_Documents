#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_bvxx.h>
#include <ADAPT/MVA/MVAPeakDetector.h>
#include <ADAPT/CUF/Format.h>
#include <optional>

using namespace adapt::agr2;
using namespace adapt::mva;
using namespace adapt::agr2::lit;

struct AlignParams
{
	AlignParams()
		: window_x(3000.), window_y(3000.), width_x(5), width_y(5),
		error_ax(0.02), error_ay(0.02), error_ra(std::hypot(0.02, 0.02)),
		gap(650),
		phcut({ 19, 19, 18, 18, 17, 17 }),
		phsumcut({ 38, 37, 36, 36, 35, 35 })
	{}
	double window_x;
	double window_y;

	double width_x;
	double width_y;

	double error_ax;
	double error_ay;
	double error_ra;
	double gap;

	std::vector<ArbValue> phcut;
	std::vector<ArbValue> phsumcut;
};

//bt1とbt2をAlignParamの条件で接続し、その位置ずれの2Dヒストグラム（とそのビンごとの飛跡対リスト）を返す関数。
HashmapClct ConnectByHashmap(ROTFiler& bt1, ROTFiler& bt2, const AlignParams& p)
{
	//ハッシュ化して繋ぐ関数。
	//ビン幅window_x、window_yでハッシュ化。

	//basetrack2をハッシュ化する。
	R_AGR r(bt2);
	r.SetLookupTable("PHCut", p.phcut);
	r.SetFilters("int(phv/10000)>=PHCut(sqrt(ax^2+ay^2))");
	r.SetVariates("x+" + std::to_string(p.gap) + "*ax", "y+" + std::to_string(p.gap) + "*ay", "sqrt(ax^2+ay^2)");
	HashmapClct hash = r.Hash({ { p.window_x }, { p.window_y }, { p.error_ra } });

	//basetrack1と、ハッシュ化したbasetrack2を接続する。
	RxHR_AGR rhr(bt1, 0, 0, multijoint, 1, 0, hash, 1, 0, bt2);
	//bt1はrank<0>、bt2はrank<3>となっている。
	//bt1の要素は変数名解決の優先順位から、単にxなどと書いても良いし、rank<0>:xとしても問題ない。
	//bt2の要素は、単にxだとbt1のものと扱われてしまうため、rank<3>:xとする必要がある。

	//飛跡を外挿した座標を一時変数として登録。
	rhr.SetTempVar("pl1_extr_x", "x-" + std::to_string(p.gap) + "*ax");
	rhr.SetTempVar("pl1_extr_y", "y-" + std::to_string(p.gap) + "*ay");
	rhr.SetTempVar("pl2_extr_x", "rank<3>:x+" + std::to_string(p.gap) + "*rank<3>:ax");
	rhr.SetTempVar("pl2_extr_y", "rank<3>:y+" + std::to_string(p.gap) + "*rank<3>:ay");
	//PHCut、PHSumCutを行うためのルックアップテーブルを作成。
	r.SetLookupTable("PHCut", p.phcut);
	r.SetLookupTable("PHSumCut", p.phsumcut);
	//外挿先のビンのindexは{ floor(x/window_x), floor(y/window_y), floor(radial angle/error_ra) }で表される。
	//そのビンと、その前後-+1の範囲（計27個のビン）に収まっている飛跡と接続判定を行う。
	rhr.SetMultiBinJoint(1, {
								  { "floor(pl1_extr_x/" + std::to_string(p.window_x) + ")", -1, 1 },
								  { "floor(pl1_extr_y/" + std::to_string(p.window_y) + ")", -1, 1 },
								  { "floor(sqrt(ax^2+ay^2)/" + std::to_string(p.error_ra) + ")", -1, 1 }
						 });
	rhr.SetSourceJoint(3, 2, 0);
	rhr.SetFilters(
		"int(phv/10000)>=PHCut(sqrt(ax^2+ay^2))"_f &&
		"int((phv+rank<3>:phv)/10000)>=PHSumCut(sqrt({(ax+rank<3>:ax)/2}^2+{(ay+rank<3>:ay)/2}^2))"_f &&
		"abs(pl1_extr_x-pl2_extr_x)<="_f + std::to_string(p.window_x) &&
		"abs(pl1_extr_y-pl2_extr_y)<="_f + std::to_string(p.window_y) &&
		"abs(rank<0>:ax-rank<3>:VAR0)<="_f + std::to_string(p.error_ax) &&
		"abs(rank<0>:ay-rank<3>:VAR1)<="_f + std::to_string(p.error_ay)
	);
	rhr.SetVariates("pl1_extr_x-pl2_extr_x", "pl1_extr_y-pl2_extr_y",
					"pl1_extr_x", "pl1_extr_y", "pl2_extr_x", "pl2_extr_y");
	return rhr.Hash({ { p.width_x }, { p.width_y } });
}
HashmapClct ConnectByBruteForce(ROTFiler& bt1, ROTFiler& bt2, const AlignParams& p)
{
	//ハッシュ化せず総当たりを行う場合。
	//window_x、window_yを使わない。
	RxR_AGR rhr(bt1, 0, 0, multijoint, 1, 0, bt2);
	//bt1はrank<0>、bt2はByHashmapの方と異なりrank<2>となっている。
	rhr.SetTempVar("pl1_extr_x", "x-" + std::to_string(p.gap) + "*ax");
	rhr.SetTempVar("pl1_extr_y", "y-" + std::to_string(p.gap) + "*ay");
	rhr.SetTempVar("pl2_extr_x", "rank<2>:x+" + std::to_string(p.gap) + "*rank<2>:ax");
	rhr.SetTempVar("pl2_extr_y", "rank<2>:y+" + std::to_string(p.gap) + "*rank<2>:ay");
	rhr.SetNestedLoopJoint(1);//総当たりにはNestedLoopJointを使う。
	rhr.SetSourceJoint(3, 2, 0);

	rhr.SetFilters(
		"int(phv/10000)>=PHCut(sqrt(ax^2+ay^2))"_f &&
		"int((phv+rank<3>:phv)/10000)>=PHCut(sqrt({(ax+rank<3>:ax)/2}^2+{(ay+rank<3>:ay)/2}^2))"_f &&
		"abs(pl1_extr_x-pl2_extr_x)<="_f + std::to_string(p.window_x) &&
		"abs(pl1_extr_y-pl2_extr_y)<="_f + std::to_string(p.window_y) &&
		"abs(rank<0>:ax-rank<2>:ax)<="_f + std::to_string(p.error_ax) &&
		"abs(rank<0>:ay-rank<2>:ay)<="_f + std::to_string(p.error_ay)
	);
	rhr.SetVariates("pl1_extr_x-pl2_extr_x", "pl1_extr_y-pl2_extr_y",
					"pl1_extr_x", "pl1_extr_y", "pl2_extr_x", "pl2_extr_y");
	return rhr.Hash({ { p.width_x }, { p.width_y } });
}

int main()
{
	int pl1, pl2;
	int zone1, zone2;

	AlignParams p;
	p.window_x = 3000;
	p.window_y = 3000;

	p.width_x = 5;
	p.width_y = 5;

	p.error_ax = 0.02;
	p.error_ay = 0.02;
	p.error_ra = std::hypot(p.error_ax, p.error_ay);
	p.gap = 650;

	p.phcut = { 19, 19, 18, 18, 17, 17 };
	p.phsumcut = { 38, 37, 36, 36, 35, 35 };

	Filer_bvxx bt1("path to basetrack1", pl1, zone1);
	Filer_bvxx bt2("path to basetrack2", pl2, zone2);

	bool by_hash = true;
	
	//bt1とbt2を接続し、飛跡対の位置ずれのヒストグラムを作成する。
	//接続はハッシュ化を使う方法と総当たりとがある。
	HashmapClct hist = by_hash ? ConnectByHashmap(bt1, bt2, p) : ConnectByBruteForce(bt1, bt2, p);

	//PeakDetectorを使って、最大値とその周辺のビンを拾い出す。
	MVAPeakDetector pd;
	PeakInfo pi = pd.Detect(hist);
	//ピークと判定されたビンの一覧を取得。
	const std::vector<BinIndex>& b = pi.GetBinIndexList();
	//histのうちピーク部分のビンだけを残し他を削除した、HashmapClct peakを作成する。
	HashmapClct peak = hist.Leave(b);

	HashmapClct::ConstTraverser t(peak);
	struct Func
	{
		double operator()(double a, double b, double c, double d, double p, double q,
						  double x1, double y1, double x2, double y2) const
		{
			double X = a * x2 + b * y2 + p;
			double Y = c * x2 + d * y2 + p;
			return std::sqrt((x1 - X) * (x1 - X) + (y1 - Y) * (y1 - Y));
		}
	};
	BinIndex index = pi.GetMaxBinIndex();
	//peak最大値のindexをx, yの座標値に変換する。
	//ConvertIndexToValueで戻ってくるのは、そのビンの下限、上限それぞれの座標値である。
	std::pair<double, double> peak_x_minmax = peak.GetAxisParam(0).ConvertIndexToValue(index[0]);
	std::pair<double, double> peak_y_minmax = peak.GetAxisParam(1).ConvertIndexToValue(index[1]);
	double peak_x = (peak_x_minmax.first + peak_x_minmax.second) / 2;
	double peak_y = (peak_y_minmax.first + peak_y_minmax.second) / 2;

	//affineパラメータをフィッティングするための初期値。
	std::array<double, 6> aff{ 1, 0, 0, 1, peak_x, peak_y };
	Func f;
	auto res = MVALevenbergMarquardt(f, aff, t, peak.GetMemberInfos("AXIS0", "AXIS1", "AXIS2", "AXIS3"));

	std::cout << adapt::Format("(a, b, c, d, p, q) = (%9.6lf, %9.6lf, %9.6lf, %9.6lf, %9.1lf, %9.1lf)", res[0], res[1], res[2], res[3], res[4], res[5]);
	return 0;
}