#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_bt.h>

#include <ADAPT/GPM2/GPMDerivative.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;
using namespace adapt::gpm2;

int AGR2_ExtractWithJoint_BinJoint()
{
	Filer_bt bt1("TestData\\b035.bbt");
	Filer_bt bt2("TestData\\b036.bbt");
	R_AGR r(bt2);
	r.SetLookupTable("PhCut", { 19, 19, 18, 18, 17, 17 });
	r.SetFilters("int(phv/10000)>PhCut(min(5, int((ax^2+ay^2)^0.5/0.2)))");
	r.SetVariates("x+ax*650.0", "y+ay*650.0", "(ax^2+ay^2)^0.5");
	HashmapClct hash = r.Hash({ { "x+ax*650.0"_v, 2000, -2000, 2000 },
								{ "y+ay*650.0"_v, 2000, -2000, 2000 },
								{ "(ax^2+ay^2)^0.5"_v, 0.1, -0.1, 0.1 } });

	RHR_AGR rhr(bt1, 0, 0, hash, 1, 0, bt2);
	rhr.SetBinJoint(1, { "floor((x-ax*650.0)/2000.0)", "floor((y-ay*650.0)/2000.0)", "floor(((ax^2+ay^2)^0.5)/0.1)" });
	rhr.SetSourceJoint(2, 1, 0);
	rhr.SetLookupTable("PhCut", { 19, 19, 18, 18, 17, 17 });
	rhr.SetLookupTable("PhSumCut", { 40, 40, 39, 38, 37, 36 });
	rhr.SetFilters(
		"int(phv/10000)>=PhCut(min(int((ax^2+ay^2)^0.5/0.2), 5))"_f &&
		"int((phv+r2:phv)/10000)>=PhSumCut(min(int[{(ax^2+ay^2)^0.5+(r2:ax^2+r2:ay^2)^0.5}/0.1], 5))"_f &&
		"abs(x-ax*650.0-r1:AXIS0)<2000" &&
		"abs(y-ay*650.0-r1:AXIS1)<2000" &&
		"abs(ax-r2:ax)<0.02" &&
		"abs(ay-r2:ay)<0.02");
	rhr.SetVariates("x-ax*650.0-r1:AXIS0", "y-ay*650.0-r1:AXIS1");
	HashmapClct hist = rhr.Hash({ { 20, -40, 40 }, { 20, -40, 40 } });

	GPMCanvasCM g("TestData\\Graphs\\global_align.png");
	g.SetSizeRatio(-1);
	g.SetXLabel("dx[um]");
	g.SetXLabel("dy[um]");
	g.Plot2DHistogram(hist, plot::title = "notitle");

	return 0;
}