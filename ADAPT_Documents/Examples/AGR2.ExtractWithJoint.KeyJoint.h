#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

HashmapClct AGR2_ExtractWithJoint_KeyJoint(const Filer_mfile& m1, const Filer_mfile& m2)
{
	RR_AGR rr(m1, 2, 0, m2);
	auto hashmap = m2.MakeHashmap({ "pl", "rawid" }, { -1, -1 }, { -2, -2 });
	rr.SetKeyJoint(1, hashmap, { "9", "rawid(index(pl==9))" });
	rr.SetFilters("size(phv)>9"_f  && "isgreatest(sum(int(phv/10000)))" &&
				  "exist(pl==9)" && "exist(pl==11)" &&
				  "isgreatest(sum(int(r1:phv/10000)))" &&
				  "exist(r1:pl==9)" && "exist(r1:pl==11)");
	rr.SetVariates("(ax^2+ay^2)^0.5", "exist(r1:pl==10)");
	HashmapClct eff = rr.Hash({ { 0.2 } });

	const auto& v0 = eff.GetMemberInfo("VAR0");

	BinIndex bin{ 0 };
	for (int i = 0; i <= 10; ++i)
	{
		bin[0] = i;
		auto c = eff.GetBinContent(bin);
		fprintf(stderr,
				"ang[%.1lf - %.1lf] : %5.3lf %lld %lld\n",
				c.GetBinMin(0), c.GetBinMax(0),
				c.GetAvg(v0), c.GetNum(), (int64_t)c.GetSum(v0));
	}
	return std::move(eff);
}