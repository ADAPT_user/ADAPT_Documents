#include <ADAPT/AGR2/HierarchicalTreeFiler.h>
#include <ADAPT/AGR2Filer/netscan_data_types_ui.h>

using namespace adapt::agr2;

class Filer_lt : public adapt::agr2::ROTFiler
{
public:

	Filer_lt();//IVEでプラグインを作りたい場合はデフォルトコンストラクタが必要。そうでなければ不要。
	Filer_lt(const std::string& filename, ReadMode mode = IMMEDIATE);
	void  DefStructure();//階層構造を定義する。

	virtual _linetype ReadHeader(FILE* fp) override;//ヘッダを読み出す。必ずしもオーバーライドする必要はない。
	//virtual _linetype ReadFooter(FILE* fp) override;//フッタを読み出す。必ずしもオーバーライドする必要はない。
	bool ReadBunch(FILE* fp, BranchInserter& b, _linetype buffersize) override;//buffersize分だけファイルから読み出す。

	//Read関数は、ReadHeader->ReadBranch->ReadBranch->...->ReadBranch->ReadFooterのように関数を呼び出す。
};


Filer_lt::Filer_lt()
{
	//コンストラクタの中で構造定義を行う。
	DefStructure();
}
inline Filer_lt::Filer_lt(const std::string& filename, ReadMode mode)
{
	//コンストラクタの中で構造定義と読み込みを実行する。
	DefStructure();

	//Read関数にはファイルパスを与える。
	//Read関数を呼ぶと、その内部でReadHeader、ReadBranch、ReadFooterが呼ばれる。
	//オーバーライドしていればその処理に従い、していなければその関数は何も行わない。

	//Read関数の挙動はReadModeによって変わる。
	//IMMEDIATEの場合、直ちにファイルを全て読み込み、格納する。
	//SYNCHRONOUSを与えた場合、Read関数中では読み込まず、代わりにファイル読み込みを待機するスレッドが一つ作られる。
	//その後、データの抽出やTraverserを用いた要素の走査を行った時に、それに同期して順次ファイルを読み出していく。
	//このとき、常にROTFilerのコンストラクタに与えたbuffer分ずつ読み出され、buffer分の走査を終えるごとにメモリから削除される。
	//SYNCHRONOUSは、例えばメモリに収まらないような巨大なファイルを扱いたい場合に、少しずつファイルを呼んでいく時に使う。
	//その仕様上、データ抽出や走査の際にSYNCHRONOUSは必ず連結最上位(rank==0)のコンテナでなければならず、
	//rank>=1のコンテナは必ずIMMEDIATEでなければならない。
	Read(filename, "rb", mode, 10000);
}
void Filer_lt::DefStructure()
{
	//階層構造を定義する。
	//AddLayer(n, "変数名1", ArbValue::Type1, "変数名2", ArbValue::Type2, ...);
	//とすることで、n層にType型の変数が追加される。
	//Typeとして現在許されているのは事実上Lng(int64_t)、Dbl(double)の2種。
	//ArbValueが対応している他の値を格納しても別に構わないが、Interpreterを用いての計算には対応していないのであまり意味がない。

	//0層 : Linklet要素
	AddLayer(0,
			 "pos1", ArbValue::Lng, "pos2", ArbValue::Lng,
			 "zproj", ArbValue::Dbl,
			 "dx", ArbValue::Dbl, "dy", ArbValue::Dbl,
			 "xc", ArbValue::Dbl, "yc", ArbValue::Dbl);

	//1層 : BaseTrack要素
	AddLayer(1,
			 "pl_b", ArbValue::Lng, "rawid_b", ArbValue::Lng,
			 "isg_b", ArbValue::Lng, "zone_b", ArbValue::Lng, "phv_b", ArbValue::Lng,
			 "ax_b", ArbValue::Dbl, "ay_b", ArbValue::Dbl,
			 "x_b", ArbValue::Dbl, "y_b", ArbValue::Dbl, "z_b", ArbValue::Dbl);

	//2層 : MicroTrack要素
	AddLayer(2,
			 "pos_m", ArbValue::Lng, "rawid_m", ArbValue::Lng,
			 "isg_m", ArbValue::Lng, "zone_m", ArbValue::Lng, "phv_m", ArbValue::Lng,
			 "ax_m", ArbValue::Dbl, "ay_m", ArbValue::Dbl, "z_m", ArbValue::Dbl);

	VerifyStructure();//最後に必ず呼ぶ。
}

_linetype Filer_lt::ReadHeader(FILE* fp)
{
	//ファイルのヘッダを読み込むための関数。
	//ReadHeader関数は必ずしもオーバーライドしなくてよい。
	//戻り値に0でない数値を返すと、予め0層要素としてその数値分のメモリを確保してくれる。
	//今回は0層要素であるlinkletの本数をファイルポインタから計算し、その数を返している。
	fpos_t fsize = 0;
	_fseeki64(fp, 0, SEEK_END);
	fgetpos(fp, &fsize);
	_linetype rowsize = (_linetype)(fsize / sizeof(linklet_t));
	std::cerr << "Reserved " << rowsize << " for branches." << std::endl;

	_fseeki64(fp, 0, SEEK_SET);

	return rowsize;
}

bool Filer_lt::ReadBunch(FILE* fp, BranchInserter& bi, _linetype buffersize)
{
	//ReadBranch関数は、0層要素（今回はLinklet）単位で計buffersize本だけファイルから読み出し、
	//それをROTFilerに格納するという動作が期待される。
	//ここではlinkletを一本ずつ読み出してBranchInserterによって格納するという動作を、
	//計buffersize回繰り返している
	//ファイルを末端まで読み終えた場合はfalseを、途中であればtrueを返す。

	//なお、ROTFiler側はファイルポインタに対してオープンとクローズのみを行い、それ以外の操作をすることはない。
	//引数のfpは、ReadHeaderや直前のReadBranchによってユーザーが操作したときの状態がそのまま保存されている。

	//BranchInserterは要素一つ一つを代入していくための一時的な容器である。
	//0層から最大階層までの各要素に一つずつの一時的な格納スペースを持ち、
	//ある層の要素一つ分を格納する毎に"Move"または"Copy"関数を呼ぶことで親要素に代入する。
	//Linklet一つ分格納し終えたら、PushBranchで本体に格納する。

	linklet_t l;
	for (_linetype i = 0; i < buffersize; ++i)
	{
		if (fread(&l, sizeof(linklet_t), 1, fp) != 1)
		{
			if (feof(fp) == 0) std::cerr << "ERROR : file reading failed." << std::endl;
			return false;
		}
		//0層の要素に値を代入する。
		//第1引数はlayerで、今回は0層に代入するので0を与える。第2引数は0層の場合は使われないのでとりあえず0としている。
		//第3引数以降はAddLayer関数で定義した変数を順次。AddLayerで定義した変数の型と代入する値の型は一致させることを推奨する（一致していなくとも動くが、将来的な修正によってバグとなる可能性がある）。
		bi.InsertValues(0, 0, (int64_t)l.pos[0], (int64_t)l.pos[1], l.zproj,
						l.dx, l.dy, l.xc, l.yc);

		//******ここからは1層以下の要素がある場合のみ必要******//

		//1層のBaseTrack要素数分だけ、スペースを確保しておく。
		//第1引数はlayerで、ここでは1層の要素数を確保するので1を与える。
		//第2引数は確保する要素数。basetrack2本分確保している。
		bi.ResizeBranch(1, 2);

		//全BaseTrackに対してループしつつ代入する。
		for (int i = 0; i < 2; ++i)
		{
			base_track_t& b = l.b[i];

			//1層の要素に値を代入する。
			//1層以下のInsertValuesでは第2引数にindexを与えなければならない。
			//indexとはつまり、現在参照しているBaseTrackが、親Linkletの持つ"0から数えて何番目の"BaseTrackであるかに等しい。
			//よって、ここではiをそのまま与える。
			bi.InsertValues(1, i, (int64_t)b.pl, (int64_t)b.rawid,
				(int64_t)b.isg, (int64_t)b.zone,
							(int64_t)(b.m[0].ph + b.m[0].ph), b.ax, b.ay,
							b.x, b.y, b.z);


			//******ここからは2層以下の要素がある場合のみ必要******//

			//MicroTrackの要素数分スペースを確保する。
			//引数の意味は上に同じ。
			bi.ResizeBranch(2, 2);
			for (int j = 0; j < 2; ++j)
			{
				micro_track_subset_t& m = b.m[j];

				//2層の要素に値を代入する。1層の時と基本的に同じである。
				//注意すべきは、indexは"親BaseTrackの持つ何番目の"MicroTrackかであり、
				//"Linkletから見て何番目か"ではない、という点である。
				//なので、i * 2 + jなどと与えてはいけない。
				bi.InsertValues(2, j, (int64_t)m.pos, (int64_t)m.rawid,
					(int64_t)m.isg, (int64_t)m.zone,
								m.ph, m.ax, m.ay, m.z);

				//もし3層以下の要素がある場合は、ここで
				//ResizeBranch、
				//ループしつつInsertValues、
				//Move、
				//の手順を1層や2層の場合と同様に行う。
			}
			//ある層の要素を全て代入したら、纏めて親要素に所有権を移す。
			//第1引数は代入し終えた要素の階層、第2引数は親要素のindex。
			//ここでは2層の値を代入し終えたので、i番目のBaseTrackにMove関数で所有権を移している。
			bi.Move(2, i);

			//******ここまで、2層以下の要素がある場合のみ必要******//
		}
		//MicroTrackと同じく、1層の要素の所有権を0番目のLinkletに移している。
		//0層の要素は0番目しか存在しないので、第2引数は何を与えても良い。
		bi.Move(1, 0);

		//******ここまで、1層以下の要素がある場合のみ必要******//

		//全ての代入を終えたらPushBranchを呼ぶ。
		//0層要素の末尾にbiが加えられる。
		PushBranch(bi);
	}

	return true;
}

Filer_lt AGR2_ReadFile_MultiLayer()
{
	return Filer_lt("");
}