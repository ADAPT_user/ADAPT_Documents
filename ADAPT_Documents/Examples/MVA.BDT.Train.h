#include <ADAPT/MVA/MVABoost.h>
#include <ADAPT/AGR2Filer/Filer_bt.h>
#include <ADAPT/AGR2/GenericAGR.h>

using namespace adapt;
using namespace agr2;
using namespace mva;

int MVA_BDT_Train()
{
	Filer_bt signal("TestData\\signal_basetracks.bbt");
	Filer_bt noise("TestData\\noise_basetracks.bbt");

	R_AGR sr(signal);
	sr.SetVariates("int(phv_m(0)/10000)", "int(phv_m(1)/10000)",
				  "int(phv_m(0)%10000)", "int(phv_m(1)%10000)",
				  "ax", "ax-ax_m(0)", "ax-ax_m(1)",
				  "ay", "ay-ay_m(0)", "ay-ay_m(1)");
	ExtractClct e_signal = sr.Extract();

	R_AGR nr(signal);
	nr.SetVariates("int(phv_m(0)/10000)", "int(phv_m(1)/10000)",
				  "int(phv_m(0)%10000)", "int(phv_m(1)%10000)",
				  "ax", "ax-ax_m(0)", "ax-ax_m(1)",
				  "ay", "ay-ay_m(0)", "ay-ay_m(1)");
	ExtractClct e_noise = nr.Extract();

	MVABoost b;
	b.Train(e_signal, e_noise,
			{ "VAR0", "VAR1", "VAR2", "VAR3", "VAR4", "VAR5", "VAR6", "VAR7", "VAR8", "VAR9" },//signalの変数
			{ "VAR0", "VAR1", "VAR2", "VAR3", "VAR4", "VAR5", "VAR6", "VAR7", "VAR8", "VAR9" },//noiseの変数
			"TestData\\BDTResult\\");

	return 0;
}