#include <ADAPT/MVA/MVABoost.h>
#include <ADAPT/AGR2Filer/Filer_bt.h>

using namespace adapt;
using namespace agr2;
using namespace mva;

int MVA_BDT_Predict()
{
	//もしTrainを呼び出したMVABoostが既にあるのなら、model.yamlを読ませることなく直接そのインスタンスを用いてもよい。
	MVABoost b("TestData\\BDTResult\\model.yaml");
	std::vector<double> predictor(10);
	//predictorにはTrainに与えたのと同様の変数を同じ順序で格納する。
	base_track_t bt;
	predictor[0] = int(bt.m[0].ph / 10000);
	predictor[1] = int(bt.m[0].ph % 10000);
	predictor[2] = int(bt.m[1].ph / 10000);
	predictor[3] = int(bt.m[1].ph % 10000);
	predictor[4] = bt.ax;
	predictor[5] = bt.ay;
	predictor[6] = bt.ax - bt.m[0].ax;
	predictor[7] = bt.ax - bt.m[1].ax;
	predictor[8] = bt.ay - bt.m[0].ay;
	predictor[9] = bt.ay - bt.m[1].ay;

	//std::vectorを用いる方がOpenCVに詳しくなくとも扱えるので楽だろうと思うが、
	//内部で一旦cv::Matへと変換するので、少しオーバーヘッドがある。
	//よって、OpenCVをある程度理解している人はcv::Mat形式で直接与える方がおそらく少しだけ高速である。
	//cv::Mat predictor = cv::Mat(1, num_of_variables, CV_32FC1);

	float response = b.PredictRawVal(predictor);
	float label = b.PredictLabel(predictor);

	return 0;
}