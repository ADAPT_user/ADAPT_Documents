#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_bt.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

int main()
{
	Filer_bt bt1("path to basetrack1");
	Filer_bt bt2("path to basetrack2");

	R_AGR r(bt2);
	r.SetVariates("x+650.0*ax", "y+650.0*ay", "sqrt(ax^2+ay^2)", "ax", "ay");
	HashmapClct hash = r.Hash({ { 150 }, { 150 }, { 0.15 } });

	RxHR_AGR rhr(bt1, 0, 0, multijoint, 1, 0, hash, 1, 0, bt2);
	rhr.SetTempVar("pl1_extr_x", "x-650.0*ax");
	rhr.SetTempVar("pl1_extr_y", "y-650.0*ay");
	rhr.SetMultiBinJoint(1, {
								  { "floor(pl1_extr_x/150.0)", -1, 1 },
								  { "floor(pl1_extr_y/150.0)", -1, 1 },
								  { "floor(sqrt(ax^2+ay^2)/0.15)", -1, 1 }
								});
	rhr.SetSourceJoint(3, 2, 0);

	rhr.SetFilters(
		"abs(pl1_extr_x-rank<2>:AXIS0)<=150"_f &&
		"abs(pl1_extr_y-rank<2>:AXIS1)<=150"_f &&
		"abs(rank<0>:ax-rank<2>:VAR0)<=0.15" &&
		"abs(rank<0>:ay-rank<2>:VAR1)<=0.15"
	);
	rhr.SetVariates("pl1_extr_x-rank<3>:x-650.0*rank<3>:ax", "pl1_extr_y-rank<3>:y-650.0*rank<3>:ay");
	ExtractClct e = rhr.Extract();

	ERR_Traverser errt(e, 2, 0, bt1, 0, 0, bt2, 0);
	errt.SetSourceJoint(1, 0, 0);
	errt.SetSourceJoint(2, 0, 3);
	auto ph1 = errt.GetMemberInfo<1>("phv");
	auto x1 = errt.GetMemberInfo<1>("x");
	auto y1 = errt.GetMemberInfo<1>("y");
	auto ax1 = errt.GetMemberInfo<1>("ax");
	auto ay1 = errt.GetMemberInfo<1>("ay");
	auto ph2 = errt.GetMemberInfo<1>("phv");
	auto x2 = errt.GetMemberInfo<1>("x");
	auto y2 = errt.GetMemberInfo<1>("y");
	auto ax2 = errt.GetMemberInfo<1>("ax");
	auto ay2 = errt.GetMemberInfo<1>("ay");
	errt.Begin();//JoinedTraverserは走査開始前にBegin関数を呼ぶ必要がある。
	for (; !errt.IsOver(); errt.Next())
	{
		fprintf(stderr, "%d %9.1lf %9.1lf %7.4lf &7.4lf %d %9.1lf %9.1lf %7.4lf &7.4lf\n",
				errt[ph1].i64(), errt[x1].f64(), errt[y1].f64(), errt[ax1].f64(), errt[ay1].f64(),
				errt[ph2].i64(), errt[x2].f64(), errt[y2].f64(), errt[ax2].f64(), errt[ay2].f64());
	}

	return 0;
}