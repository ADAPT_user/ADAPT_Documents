#include "pch.h"

#include "CUF.KeywordArgs.h"
#include "CUF.ThreadPool.h"

#include "AGR2.ReadFile.MultiLayer.h"
#include "AGR2.ReadFile.SingleLayer.h"
#include "AGR2.Traverse.Sequential.h"
#include "AGR2.Traverse.Random.h"
#include "AGR2.Extract.h"
#include "AGR2.Extract.ExtractSeparately.h"
#include "AGR2.ExtractWithJoint.h"
#include "AGR2.ExtractWithJoint.BinJoint.h"
#include "AGR2.ExtractWithJoint.KeyJoint.h"
#include "AGR2.ExtractWithJoint.SourceJoint.h"
#include "AGR2.Hash.h"
#include "AGR2.Hash.Section.h"
#include "AGR2.Hash.Projection.h"

#include "GPM2.Histogram1D.h"

#include "MVA.BDT.train.h"
#include "MVA.BDT.predict.h"

//CUF

//KeywordArgs
TEST(CUF, KeywordArgs)
{
	CUF_KeywordArgs();
}

//ThreadPool
TEST(CUF, ThreadPool)
{
	CUF_ThreadPool();
}


//AGR2

class AGR2 : public ::testing::Test
{
protected:

	virtual void SetUp() override
	{
		mfile = std::make_unique<Filer_mfile>("TestData\\test_mfile.all");
		mfile2 = std::make_unique<Filer_mfile>("TestData\\test_mfile2.all");
		bt = std::make_unique<Filer_bt>("TestData\\signal_basetracks.bbt");
		bt2 = std::make_unique<Filer_bt>("TestData\\noise_basetracks.bbt");
	}

	std::unique_ptr<Filer_mfile> mfile;
	std::unique_ptr<Filer_mfile> mfile2;
	std::unique_ptr<Filer_bt> bt;
	std::unique_ptr<Filer_bt> bt2;
};

//ReadFile
TEST_F(AGR2, ReadFile_SingleLayer)
{
	AGR2_ReadFile_SingleLayer();
}
TEST_F(AGR2, ReadFile_MultiLayer)
{
	//AGR2_ReadFile();
}

//Traverse
TEST_F(AGR2, Traverse_Sqquential)
{
	AGR2_Traverse_Sequential(*mfile);
}
TEST_F(AGR2, Traverse_Random)
{
	AGR2_Traverse_Random(*mfile);
}

//Extract
TEST_F(AGR2, Extract)
{
	AGR2_Extract(*mfile);
}
TEST_F(AGR2, ExtractSeparately)
{
	AGR2_Extract_ExtractSeparately(*mfile);
}

//Hash
TEST_F(AGR2, Hash)
{
	AGR2_Hash(*mfile);
}
TEST_F(AGR2, Hash_Projection)
{
	AGR2_Hash_Projection(*mfile);
}

//ExtractWithJoint
TEST_F(AGR2, ExtractWithJoint)
{
	AGR2_ExtractWithJoint(*mfile, *bt);
}
TEST_F(AGR2, ExtractWithJoint_BinJoint)
{
	AGR2_ExtractWithJoint_BinJoint();
}
TEST_F(AGR2, ExtractWithJoint_KeyJoint)
{
	AGR2_ExtractWithJoint_KeyJoint(*mfile, *mfile2);
}
TEST_F(AGR2, ExtractWithJoint_SourceJoint)
{
	AGR2_ExtractWithJoint_SourceJoint(*mfile);
}


//GPM2

class GPM2 : public ::testing::Test
{
protected:

	virtual void SetUp() override
	{
		bt = std::make_unique<Filer_bt>("TestData\\signal_basetracks.bbt");
		bt2 = std::make_unique<Filer_bt>("TestData\\noise_basetracks.bbt");
	}

	std::unique_ptr<Filer_bt> bt;
	std::unique_ptr<Filer_bt> bt2;
};

//Histogram1D
TEST_F(GPM2, Histogram1D)
{
	GPM2_Histogram1D(*bt, *bt2);
}


//MVA

//Machine Learning
TEST(MVA, BDT_Train)
{
	MVA_BDT_Train();
}
TEST(MVA, BDT_Predict)
{
	MVA_BDT_Predict();
}