#ifndef AGR2_READ_FILE_SINGLE_LAYER_H
#define AGR2_READ_FILE_SINGLE_LAYER_H

#include <ADAPT/AGR2/HierarchicalTreeFiler.h>
#include <ADAPT/AGR2Filer/netscan_data_types_ui.h>

using namespace adapt::agr2;

ROTFiler AGR2_ReadFile_SingleLayer()
{
	std::string path = "TestData\\signal_basetracks.bbt";
	ROTFiler f;
	f.AddLayer(0, "rawid", ArbValue::I64, "pl", ArbValue::I64, "phv", ArbValue::I64,
			   "ax", ArbValue::F64, "ay", ArbValue::F64, "x", ArbValue::F64, "y", ArbValue::F64, "z", ArbValue::F64);
	f.VerifyStructure();

	base_track_t b;
	FILE* fp = fopen(path.c_str(), "rb");
	if (fp == nullptr)
	{
		fprintf(stderr, "file cannot open.\n");
		return std::move(f);
	}
	Bpos bpos;
	ROTFiler::Branch br(f);
	while (fread(&b, sizeof(base_track_t), 1, fp) == 1)
	{
		br.SetValues(bpos, b.rawid, b.pl, b.m[0].ph + b.m[1].ph, b.ax, b.ay, b.x, b.y, b.z);
		f.PushBranch(br);
	}
	return std::move(f);
}

#endif