#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

int AGR2_Traverse_Random(const Filer_mfile& m)
{
	R_AGR agr(m);
	agr.SetFilters("int(phv/10000)>=20"_f && "sum(int(phv/10000))>=50"_f);
	agr.SetVariates("sum(int(phv/10000))", "phv", "greatest(size(ax))", "size(ax)", " (ax^2+ay^2)^0.5");
	ExtractClct e = agr.Extract();

	auto [v0, v1, v2, v3, v4] = e.GetMemberInfos("VAR0", "VAR1", "VAR2", "VAR3", "VAR4");

	return 0;
}