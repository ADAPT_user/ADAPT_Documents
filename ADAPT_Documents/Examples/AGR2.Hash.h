#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

HashmapClct AGR2_Hash(const Filer_mfile& m)
{
	R_AGR agr(m);
	agr.SetFilters("sum(int(phv/10000))>=50"_f && "isfirst(phv)==0" && "islast(phv)==0"_f);
	agr.SetVariates("ax", "ay", "int(phv/10000)", "int(phv%10000)", "size(ax)");
	HashmapClct h = agr.Hash({ AxisParam(0.05), AxisParam(0.05) });

	HashmapClct::ConstTraverser t(h);
	const auto& v0 = h.GetMemberInfo("AXIS0");
	const auto& v1 = h.GetMemberInfo("AXIS1");
	const auto& v2 = h.GetMemberInfo("VAR0");
	const auto& v3 = h.GetMemberInfo("VAR1");
	const auto& v4 = h.GetMemberInfo("VAR2");
	for (int row = 0; !t.IsOver(); t.Next(), ++row)
	{
		fprintf(stderr, "row[%4d] : %7.4lf %7.4lf %2lld %4lld %2lld\n",
				row, t[v0].f64(), t[v1].f64(), t[v2].i64(), t[v3].i64(), t[v4].i64());
	}

	BinIndex binmin = h.GetBinMin();
	BinIndex binmax = h.GetBinMax();
	BinIndex bin{ 0, 0 };
	for (int i = binmin[0]; i <= binmax[0]; ++i)
	{
		for (int j = binmin[1]; j <= binmax[1]; ++j)
		{
			bin[0] = i;
			bin[1] = j;
			const auto& c = h.GetBinContent(bin);
			size_t num = c.GetNum();
			fprintf(stderr, "Num of Elements : %zu\n", num);
			for (_cpostype k = 0; k < num; ++k)
			{
				fprintf(stderr,
						"bin[%4d][%4d] : %7.4lf %7.4lf %2lld %4lld %2lld\n",
						bin[0], bin[1],
						c.GetValue(v0, k).f64(), c.GetValue(v1, k).f64(),
						c.GetValue(v2, k).i64(), c.GetValue(v3, k).i64(), c.GetValue(v4, k).i64());
			}
		}
	}

	return std::move(h);
}