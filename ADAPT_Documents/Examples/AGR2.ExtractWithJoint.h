#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>
#include <ADAPT/AGR2Filer/Filer_bt.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

ExtractClct AGR2_ExtractWithJoint(const Filer_mfile& m, const Filer_bt& b)
{
	RR_AGR rr(m, 2, 0, b);
	auto hashmap = b.MakeHashmap({ "pl", "rawid" }, { -1, -1 }, { -2, -2 });
	rr.SetKeyJoint(1, hashmap, { "pl", "rawid" });
	rr.SetFilters("size(phv)>5"_f && "isgreatest(sum(int(phv/10000)))");
	rr.SetVariates("r1:ax-ax_m(0)", "r1:ax-ax_m(1)", "r1:ay-ay_m(0)", "r1:ay-ay_m(1)");
	ExtractClct e = rr.Extract();

	ExtractClct::ConstTraverser t(e);
	const auto& v0 = e.GetMemberInfo("VAR0");
	const auto& v1 = e.GetMemberInfo("VAR1");
	const auto& v2 = e.GetMemberInfo("VAR2");
	const auto& v3 = e.GetMemberInfo("VAR3");
	for (; !t.IsOver(); t.Next())
	{
		fprintf(stderr, "%7.4lf %7.4lf %7.4lf %7.4lf\n",
				t[v0].f64(), t[v1].f64(), t[v2].f64(), t[v3].f64());
	}

	return std::move(e);
}