#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

std::tuple<ExtractClct, ExtractClct> AGR2_ExtractWithJoint_SourceJoint(const Filer_mfile& mfile)
{
	R_AGR r(mfile);
	r.SetFilters("size(phv)>5"_f && "isgreatest(sum(int(phv/10000)))" && "int(phv/10000)>=20");
	r.SetVariates("size(phv)", "greatest(sum(int(phv/10000)))", "GroupID", "ChainID", "pl", "rawid");
	ExtractClct e = r.Extract();

	{
		ExtractClct::ConstTraverser t(e);
		const ValueInfo& v0 = e.GetMemberInfo("VAR0");
		const ValueInfo& v1 = e.GetMemberInfo("VAR1");
		const ValueInfo& v2 = e.GetMemberInfo("VAR2");
		const ValueInfo& v3 = e.GetMemberInfo("VAR3");
		const ValueInfo& v4 = e.GetMemberInfo("VAR4");
		const ValueInfo& v5 = e.GetMemberInfo("VAR5");
		for (; !t.IsOver(); t.Next())
		{
			fprintf(stderr, "%lld %lld %lld %lld %lld %lld\n", t[v0].i64(), t[v1].i64(), t[v2].i64(), t[v3].i64(), t[v4].i64(), t[v5].i64());
		}
	}

	ER_AGR er(e, 2, 2, mfile);
	er.SetSourceJoint(1, 0, 0);
	er.SetVariates("VAR0", "size(phv)", "rank<1>:size(phv)",
				   "VAR1", "greatest(sum(int(phv/10000)))", "rank<1>:greatest(rank<1>:sum(int(phv/10000)))");
	ExtractClct ee = er.Extract();
	{
		ExtractClct::ConstTraverser tt(ee);
		const ValueInfo& v0 = ee.GetMemberInfo("VAR0");
		const ValueInfo& v1 = ee.GetMemberInfo("VAR1");
		const ValueInfo& v2 = ee.GetMemberInfo("VAR2");
		const ValueInfo& v3 = ee.GetMemberInfo("VAR3");
		const ValueInfo& v4 = ee.GetMemberInfo("VAR4");
		const ValueInfo& v5 = ee.GetMemberInfo("VAR5");
		for (; !tt.IsOver(); tt.Next())
		{
			fprintf(stderr, "%lld %lld %lld %lld %lld %lld\n", tt[v0].i64(), tt[v1].i64(), tt[v2].i64(), tt[v3].i64(), tt[v4].i64(), tt[v5].i64());
		}
	}
	return std::make_tuple(std::move(e), std::move(ee));
}