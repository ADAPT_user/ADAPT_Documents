import adapt.agr2 as agr
import struct

def Create(path):
    #resは戻り値。ここにデータを格納する。
    res = agr.ROTFiler()
    #まず0、1層それぞれの変数を定義する。変数名と型(整数ならI64、浮動小数点ならF64)をそれぞれ与える。
    res.AddLayer(0, [ ("phv", agr.ArbValue.Tag.I64), ("ax", agr.ArbValue.Tag.F64), ("ay", agr.ArbValue.Tag.F64), \
                      ("x", agr.ArbValue.Tag.F64), ("y", agr.ArbValue.Tag.F64), ("z", agr.ArbValue.Tag.F64), \
                      ("pl", agr.ArbValue.Tag.I64), ("isg", agr.ArbValue.Tag.I64), ("zone", agr.ArbValue.Tag.I64), ("rawid", agr.ArbValue.Tag.I64) ])
    res.AddLayer(1, [ ("phv_m", agr.ArbValue.Tag.I64), ("ax_m", agr.ArbValue.Tag.F64), ("ay_m", agr.ArbValue.Tag.F64), ("z_m", agr.ArbValue.Tag.F64), \
                      ("pos_m", agr.ArbValue.Tag.I64), ("isg_m", agr.ArbValue.Tag.I64), ("zone_m", agr.ArbValue.Tag.I64), ("rawid_m", agr.ArbValue.Tag.I64), \
                      ("row_m", agr.ArbValue.Tag.I64), ("col_m", agr.ArbValue.Tag.I64) ])
    #定義を終えたらVerifyStructure()を呼ぶ。
    res.VerifyStructure()

    #ROTFiler.Branchのオブジェクトに一度データを格納し、それをresへと押し込むという流れを取るので、
    #まずBranchオブジェクトbrを用意する。
    br = agr.ROTFiler.Branch(res)
    #層数2のコンテナの位置情報を格納するためのBposを用意しておく。
    #イメージとしては、std::vector<std::vector<...>>の要素を示す2対のindexのようなもの。
    #bpos[0]が0層の、bpos[1]が1層のindex。
    #0層のindex、つまりbpos[0]は、Branchにデータを格納するときは常に0でよい。
    bpos = agr.Bpos(2, 0)#第1引数は層数、第2引数は各層のindexの初期値。2層分のindexを確保し全て0で初期化する、の意味。

    f = open(path, "rb")
    struct_fmt = "dddddiiiiqdddiiiiiiqdddiiiiiiq"
    struct_len = struct.calcsize(struct_fmt)
    struct_unpack = struct.Struct(struct_fmt).unpack_from

    while True:
        data = f.read(struct_len)
        if not data: break

        #basetrackをbrに格納する。
        bt = struct_unpack(data)
        arr = [ bt[13] + bt[23], bt[0], bt[1], bt[2], bt[3], bt[4], bt[5], bt[6], bt[7], bt[9] ]
        br.SetValues(0, bpos, arr)

        #1層の大きさを変更する。今回はmicrotrackなので、2本分固定でよい。
        br.Resize(1, bpos, 2)
        for i in range(2):
            #microtrackをbrに格納する。
            bpos[1] = i
            offs = i * 10
            arr = [ bt[13 + offs], bt[10 + offs], bt[11 + offs], bt[12 + offs], \
                    bt[14 + offs], bt[18 + offs], bt[17 + offs], bt[19 + offs], bt[16 + offs], bt[15 + offs] ]
            br.SetValues(1, bpos, arr)
        res.PushBranch(br)
    return res