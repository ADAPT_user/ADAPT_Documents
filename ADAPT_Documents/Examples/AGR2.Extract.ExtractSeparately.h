#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

std::vector<ExtractClct> AGR2_Extract_ExtractSeparately(const Filer_mfile& m)
{
	R_AGR agr(m);
	agr.SetSeparator("greatest(size(phv))>=4");
	agr.SetVariates("GroupID");
	std::vector<ExtractClct> e = agr.ExtractSeparately(2);

	return std::move(e);
}