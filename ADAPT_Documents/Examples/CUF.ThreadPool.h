#include <ADAPT/CUF/ThreadPool.h>
#include <numeric>
#include <chrono>
#include <random>

using namespace adapt;

int Func1(int s)
{
	//sミリ秒間待機するだけの関数。
	std::this_thread::sleep_for(std::chrono::milliseconds(s));
	std::cout << s << " seconds" << std::endl;
	return s;
}
double Func2(const std::vector<double>& v1, int pow)
{
	//v1[0]^pow + v1[1]^pow + v2[2]^pow + ...
	return std::transform_reduce(
		v1.begin(), v1.end(), 0.,
		[](double a, double b) { return a + b; },
		[pow](double a) { return std::pow(a, pow); });
}

int CUF_ThreadPool()
{
	ThreadPool jm(4);

	{
		//例1。ただ単に関数を呼び出す。
		for (int i = 0; i < 10; ++i)
		{
			//引数に与えたiはそのままFunc1へと送られる。
			jm.AddTask(&Func1, i);
		}
		jm.Join();
	}

	{
		jm.Start(4);
		//例2。引数を参照型として渡す。また関数の戻り値を受け取りたい場合。
		std::vector<double> v1(10);
		std::iota(v1.begin(), v1.end(), 0);
		std::vector<std::future<double>> res;
		for (int i = 0; i < 10; ++i)
		{
			res.push_back(jm.AddTask(&Func2, std::cref(v1), i));
		}
		jm.Join();
		for (auto& r : res) std::cout << "sum of vector = " << r.get() << std::endl;
	}
	
	{
		jm.Start(4);
		//例3。関数オブジェクトやラムダ式を使う場合。ついでにmoveして渡す場合。
		auto f = [](std::unique_ptr<int> x, std::unique_ptr<int> y) -> std::unique_ptr<int>
		{
			return std::make_unique<int>(*x + *y);
		};
		std::unique_ptr<int> x = std::make_unique<int>(4);
		std::unique_ptr<int> y = std::make_unique<int>(6);
		std::future<std::unique_ptr<int>> res = jm.AddTask(f, std::move(x), std::move(y));
		std::cout << *res.get() << std::endl;
		jm.Join();
	}

	return 0;
}