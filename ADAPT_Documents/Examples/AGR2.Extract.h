#include <ADAPT/AGR2/GenericAGR.h>
#include <ADAPT/AGR2Filer/Filer_mfile.h>

using namespace adapt::agr2;
using namespace adapt::agr2::lit;

ExtractClct AGR2_Extract(const Filer_mfile& m)
{
	R_AGR agr(m);
	agr.SetFilters("int(phv/10000)>=20"_f && "sum(int(phv/10000))>=50"_f);
	agr.SetVariates("sum(int(phv/10000))", "phv", "greatest(size(ax))", "size(ax)", " (ax^2+ay^2)^0.5");
	ExtractClct e = agr.Extract();

	return std::move(e);
}