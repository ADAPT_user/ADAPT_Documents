#include <ADAPT/CUF/KeywordArgs.h>
#include <vector>

using namespace adapt;

namespace opts
{
CUF_DEFINE_KEYWORD_OPTION(non_type)
CUF_DEFINE_KEYWORD_OPTION_WITH_VALUE(floating_point, float)
CUF_DEFINE_KEYWORD_OPTION_WITH_VALUE(rvector, std::vector<double>&&)
CUF_DEFINE_KEYWORD_OPTION_WITH_VALUE(lvector, std::vector<double>&)
}
template <class ...Args>
void func(Args ...args)
{
	//キーワードの有無のみを調べる。下のmain関数では与えられているので、trueとなる。
	std::cout << "Non_type       : " << KeywordExists(opts::non_type, args...) << std::endl;

	//引数を指定されなかったため末尾のデフォルト値が返る。
	std::cout << "Floating point : " << GetKeywordArg(opts::floating_point, args..., 3.0f) << std::endl;

	auto rv = GetKeywordArg(opts::rvector, args...);
	std::cout << "RVector        :";
	for (auto& vv : rv) std::cout << " " << vv;
	std::cout << std::endl;

	auto& lv = GetKeywordArg(opts::lvector, args...);
	std::cout << "LVector        :";
	for (auto& vv : lv) std::cout << " " << vv;
	std::cout << std::endl;
	for (int i = 0; i < lv.size(); ++i) lv[i] = i * 3;
}

namespace opts
{
struct Tag1 {};
struct Tag2 : public Tag1 {};
CUF_DEFINE_TAGGED_KEYWORD_OPTION_WITH_VALUE(integer, int, Tag1)
CUF_DEFINE_TAGGED_KEYWORD_OPTION_WITH_VALUE(string, const std::string&, Tag2)
}
template <class ...Args, CUF_TAGGED_ARGS_ENABLER(Args, opts::Tag1)>//Tag1のキーワード引数のみ許される。
void func_tag1(Args&& ...args)
{
	std::cout << "Integer : " << GetKeywordArg(opts::integer, args..., 10) << std::endl;
}
template <class ...Args, CUF_TAGGED_ARGS_ENABLER(Args, opts::Tag2)>//Tag2とその基底クラスであるTag1のキーワード引数のみ許される。
void func_tag2(Args&& ...args)
{
	std::cout << "Integer : " << GetKeywordArg(opts::integer, args..., 10) << std::endl;
	std::cout << "String  : " << GetKeywordArg(opts::string, args..., "xyz") << std::endl;
}

int CUF_KeywordArgs()
{
	std::vector<double> r = { 5, 5, 5 };
	std::vector<double> l = { 5, 5, 5 };
	func(opts::non_type, opts::rvector = std::move(r), opts::lvector = l);
	std::cout << "RVector        :";
	for (auto& v : r) std::cout << " " << v;//moveしたので空に。
	std::cout << std::endl;
	std::cout << "LVector        :";
	for (auto& v : l) std::cout << " " << v;//左辺値参照なので中身が書き換えられる。
	std::cout << std::endl;

	std::cout << "--------------------" << std::endl;
	func_tag1(opts::integer = 4);
	//func_tag1(args::string = "abcde");
	std::cout << "--------------------" << std::endl;
	func_tag2(opts::integer = 4, opts::string = "abc");
	return 0;
}